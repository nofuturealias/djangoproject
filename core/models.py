from django.db import models

# Create your models here.


class Details(models.Model): #id key automatically banaucha
    name = models.CharField(max_length=100)
    pob = models.CharField(max_length=100,blank=True, null=True)
    edu = models.CharField(max_length=100,blank=True, null=True)
    country =models.CharField(max_length=100,blank=True, null=True)
    province = models.CharField(max_length=100,blank=True, null=True)
    district = models.CharField(max_length=100,blank=True, null=True)
    v_mun = models.CharField(max_length=100,blank=True, null=True)
    street = models.CharField(max_length=100,blank=True, null=True)
    ward = models.CharField(max_length=100,blank=True, null=True)
    houseno = models.CharField(max_length=100,blank=True, null=True)
    mobile = models.CharField(max_length=100,blank=True, null=True)
    telephone = models.CharField(max_length=100,blank=True, null=True)
    fax = models.CharField(max_length=100,blank=True, null=True)
    marital_status = models.CharField(max_length=100,blank=True, null=True)