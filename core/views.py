from django.shortcuts import render,redirect, get_object_or_404
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from .models import Details
from django.db.models import Avg
# Create your views here.

from django.http import HttpResponse
from django.views.generic import View

from core.utils import render_to_pdf #created in step 4
from .render import Render


from .models import *


from django.contrib import messages

import requests
import json



#class Pdf(View):
def pdf(request,user_id):
		x = Details.objects.get(id=user_id)
		context = {
		'data': x
		}
		#return render(request,'view.html', context) 
		return Render.render('view.html',context)


def home(request):
	x = User.objects.all()
	details = Details.objects.all()
	context = {
	'data' : x,
	'details' : details
	}
	return render(request,'index.html', context) 

@login_required
def register(request):
	if request.method == 'POST':
		name = request.POST['name']
		email = request.POST['email']
		password = request.POST['password']

		x=User.objects.create_user(
			username = name,
			email = email,
			password = password,
			)
		
		return redirect('homepage')
	else:
		return render(request,'register.html', {})
		# print(request.POST['title'] 


def loginpage(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request,username=username,password=password)
		if user is not None:
			login(request,user)
			return redirect('dashboard')
		else:
			#messages.error(request, 'Username or password incorrect')
			messages={
			"data":"Username or password incorrect"
			}
			return render(request,'login.html', messages)


	else:
		return render(request,'login.html', {})



@login_required(login_url='/loginpage')
def dashboard(request):
	if request.method == 'POST':
		name = request.POST['name']
		country = request.POST['country']
		province = request.POST['province']
		district = request.POST['district']
		v_mun = request.POST['v_mun']
		street = request.POST['street']
		pob = request.POST['pob']
		ward = request.POST['ward']
		houseno = request.POST['houseno']
		mobile = request.POST['mobile']
		telephone = request.POST['telephone']
		fax = request.POST.get('fax', False)	
		edu = request.POST['edu']
		marital_status = request.POST.get('marital_status')
		Details.objects.create(
			name = name,
			country = country,
			province = province,
			district = district,
			v_mun = v_mun,
			street = street,
			ward = ward,
			houseno = houseno,
			mobile = mobile,
			telephone = telephone,
			marital_status = marital_status,
			fax = fax,
			pob = pob,
			edu = edu
			)
		return redirect('homepage')
	else:
		return render(request,'dashboard.html', {})

@login_required(login_url='/loginpage')
def logouts(request):
	logout(request)
	return redirect('loginpage')



@login_required 
def pass_change(request):
	if request.method == 'POST':
		password1 = request.POST['password1']
		password = request.POST['password']
		if (password1==password):
			x = request.user
			change_pass = password
			x.set_password(change_pass)
			x.save()
			return redirect('homepage')
	else:
		return render(request,'changepass.html',{})


@login_required 
def requireddata(request,user_id):
	x= Details.objects.get(id = user_id)
#	x = get_object_or_404(Details, id=user_id)
	if request.method == 'POST':
		x= Details.objects.get(id = user_id)
		country1 = request.POST.get('country')
		name1 = request.POST.get('name')
		province1 = request.POST.get('province')
		district1 = request.POST.get('district')
		v_mun1 = request.POST.get('v_mun')
		street1 = request.POST.get('street')
		pob1 = request.POST.get('pob')
		ward1 = request.POST.get('ward')
		houseno1 = request.POST.get('houseno')
		mobile1 = request.POST.get('mobile')
		telephone1 = request.POST.get('telephone')
		fax1 = request.POST.get('fax', False)	
		edu1 = request.POST.get('edu')
		marital_status1 = request.POST.get('marital_status')
		x.name = name1
		x.country = country1
		x.province = province1
		x.district = district1
		x.v_mun = v_mun1
		x.street = street1
		x.ward = ward1
		x.houseno = houseno1
		x.mobile = mobile1
		x.telephone = telephone1
		x.marital_status = marital_status1
		x.fax = fax1
		x.pob = pob1
		x.edu = edu1
		x.save()
		return redirect('homepage')
	else:
		context = {
		'data' : x
		}
		return render(request,'edit.html',context)
        #FeedBack.objects.filter((id=user_id).update(title))
        #FeedBack.objects.filter((id=user_id).update(description))
        #  x.save()
          
#def pdf(request,user_id):
#	x= Details.objects.get(id=user_id)
#	context = {
#	'data' : x
#	}
#	return render(request,'view.html',context)




#class GeneratePdf(View):
 #   def get(self, request,user_id, *args, **kwargs):
  #      x= Details.objects.get(id=user_id)
   #     context = {
	#	'data' : x
	#	}
	#	return Render.render('view.html',context)
        #pdf = render_to_pdf('view.html', context)
        #return HttpResponse(pdf, content_type='application/pdf')


@login_required(login_url='/loginpage')
def dashboard2(request):
	if request.method == 'POST':
		name = request.POST['name']
		country = request.POST['country']
		province = request.POST['province']
		district = request.POST['district']
		v_mun = request.POST['v_mun']
		street = request.POST['street']
		pob = request.POST['pob']
		ward = request.POST['ward']
		houseno = request.POST['houseno']
		mobile = request.POST['mobile']
		telephone = request.POST['telephone']
		fax = request.POST.get('fax', False)	
		edu = request.POST['edu']
		marital_status = request.POST.get('marital_status')
		Details.objects.create(
			name = name,
			country = country,
			province = province,
			district = district,
			v_mun = v_mun,
			street = street,
			ward = ward,
			houseno = houseno,
			mobile = mobile,
			telephone = telephone,
			marital_status = marital_status,
			fax = fax,
			pob = pob,
			edu = edu
			)
		return redirect('homepage')
	else:
		return render(request,'dashboard2.html', {})

    
       	
@login_required
def admin(request):
	data_number = Details.objects.all().count()
	data1 = Details.objects.all().aggregate(Avg('mobile')) 
	data2 = Details.objects.all().aggregate(Avg('telephone'))
	return render(request,'admin_index.html',{'data_number':data_number,'data1':data1,'data2':data2})   

def coviddata(request):
	if request.method == 'POST':
		country = request.POST['country']
		url = "https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/total"

		querystring = {"country":country}

		headers = {
    	'x-rapidapi-host': "covid-19-coronavirus-statistics.p.rapidapi.com",
    	'x-rapidapi-key': "bba5e60150mshc35a4a89f4f7413p1b3a26jsnf8fdeeb864fd"
   		 }

		response = requests.request("GET", url, headers=headers, params=querystring).json()

		data = response['data']
		print(response)
		print(data)
		print(data['recovered'])	
		context = {
			'recovered' : data['recovered'],
			'deaths' : data['deaths'],
			'confirmed' : data['confirmed'],
			'location' : data['location'],
			'lastReported' : data['lastReported']
		
		}

	
		#return HttpResponse(data['recovered'])
		return render(request,'covidselect.html', context)

	else:

		return render(request,'covidselect.html', {})


def test(request):
	return render(request,'test.html',{})	
        	
        		
       	
       
    
        

