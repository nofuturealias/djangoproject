"""login URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from core.views import register,home,loginpage,dashboard,logouts,pass_change,admin
from core.views import requireddata,pdf,dashboard2,coviddata,test


urlpatterns = [
    #path('admin/', admin.site.urls),
    #path('admin/', admin.site.urls),
    #url(r'^admin/', include(admin.site.urls)),
    path('admin1/',admin),
    path('register/', register,name='register'),
    #path('',home,name='homepage'),  kaam lagney
    #path('loginpage/', loginpage,name='loginpage'), kaam lagney
    path('loginpage/',home,name='homepage'),  
    path('coviddata/',coviddata,name='coviddata'),
    path('', loginpage,name='loginpage'), 
    path('dashboard/', dashboard,name='dashboard'),
    path('logouts/', logouts,name='logouts'),
    path('passchange/', pass_change,name='pass_change'),
    path('requireddata/<int:user_id>', requireddata,name='requireddata'),
    path('dashboard2/', dashboard2,name='dashboard2'),
    #path('pdf/<int:user_id>', pdf,name='pdf'),
    path('pdf/<int:user_id>',pdf,name='GeneratePdf'),

    path('test/',test,name='test'),
]
